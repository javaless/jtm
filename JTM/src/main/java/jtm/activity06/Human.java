package jtm.activity06;

public class Human implements Humanoid, Cloneable {
	int stomach;
	int birthWeight;
	boolean isAlive;

	public Human() {
		stomach = 0;
		birthWeight = BirthWeight;
		isAlive = true;

	}
	@Override
	public void eat(Integer food) {
		if (stomach == 0)
			stomach = food;

	}

	@Override
	public Integer vomit() {
		int tmp = stomach;
		stomach = 0;
		return tmp;
	}

	@Override
	public String isAlive() {
		if (isAlive)
			return "Alive";
		else
			return "Dead";
	}

	@Override
	public String killHimself() {
		isAlive = false;
		return isAlive();
	}

	@Override
	public int getWeight() {
		return birthWeight + stomach;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + getWeight() + " [" + stomach + "]";
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Human cloneHuman = new Human();
		cloneHuman.stomach = this.stomach;
		return cloneHuman;
	}
}
