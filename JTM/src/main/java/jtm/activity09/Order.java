package jtm.activity09;

import java.util.Objects;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 *
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 *
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 *
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 *
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 *
 */

public class Order implements Comparable<Order> {
	String customer;
	String name; 
	int count; 

	public Order(String orderer, String itemName, Integer count) {
		this.customer = orderer;
		this.name = itemName;
		this.count = count;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Order) {
			return this.compareTo((Order)other) == 0;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.customer, this.name, this.count);
	}

	@Override
	public String toString() {
		return this.name + ": " + this.customer + ": " + this.count;
	}

	@Override
	public int compareTo(Order o) {
		int customerCompare = this.customer.compareTo(o.customer);
		if (customerCompare != 0) {
			return customerCompare;
		}

		int itemCompare = this.name.compareTo(o.name);
		if (itemCompare != 0) {
			return itemCompare;
		}

		return Integer.compare(this.count, o.count);


	}


}