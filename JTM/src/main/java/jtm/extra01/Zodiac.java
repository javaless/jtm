package jtm.extra01;

public class Zodiac {

	/**
	 * Determine the sign of the zodiac, use day and month.
	 * 
	 * @param day
	 * @param month
	 * @return zodiac
	 */

//	Capricorn December 22 - January 19
//	Aquarius January 20 - February 18
//	Pisces February 19 - March 20
//	Aries March 21-April 20
//	Taurus April 21 - May 21
//	Gemini May 22 - June 21
//	Cancer June 22 - July 22
//	Leo July 23 - August 22
//	Virgo August 23 - September 22
//	Libra September 23 - October 22
//	Scorpio October 23 - November 21
//	Sagittarius November 22 - December 21

	public static String getZodiac(int day, int month) {
		String zodiac = null;
		// TODO #1: Implement method which return zodiac sign names
		// As method parameter - day and month;
		// Look at wikipedia:
		// https://en.wikipedia.org/wiki/Zodiac#Table_of_dates
		// Tropical zodiac, to get appropriate date ranges for signs

		if ((day > 21 && month == 12) || (day <= 19 && month == 1)) {
			zodiac = "Capricon";
		} else if ((day > 20 && month == 1) || (day <= 18 && month == 2)) {
			zodiac = "Aquarius";
		}  if ((day > 19 && month == 2) || (day <= 20 && month == 3)) {
			zodiac = "Pisces";
		}  if ((day > 21 && month == 3) || (day <= 20 && month == 4)) {
			zodiac = "Aries";
		}  if ((day > 21 && month == 4) || (day <= 20 && month == 5)) {
			zodiac = "Taurus";
		}  if ((day > 21 && month == 5) || (day <= 20 && month == 6)) {
			zodiac = "Gemini";
		}  if ((day > 21 && month == 6) || (day <= 20 && month == 7)) {
			zodiac = "Cancer";
		}  if ((day > 21 && month == 7) || (day <= 20 && month == 8)) {
			zodiac = "Leo";
		}  if ((day > 21 && month == 8) || (day <= 22 && month == 9)) {
			zodiac = "Virgo";
		}  if ((day > 23 && month == 9) || (day <= 20 && month == 10)) {
			zodiac = "Libra";
		}  if ((day > 21 && month == 10) || (day <= 22 && month == 11)) {
			zodiac = "Scorpio";
		}  if ((day > 23 && month == 11) || (day <= 20 && month == 12)) {
			zodiac = "Sagitarius";
		}
		return zodiac;

	}

	public static void main(String[] args) {
		// HINT: you can use main method to test your getZodiac method with
		// different parameters
        
		System.out.println(getZodiac(2,1));
	}

}
